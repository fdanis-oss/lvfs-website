# Custom template
"""empty message

Revision ID: 90fb9c7b5f1a
Revises: 3bf16c5c54bb
Create Date: 2023-01-27 11:50:34.629891

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "90fb9c7b5f1a"
down_revision = "3bf16c5c54bb"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "firmware_assets",
        sa.Column("firmware_asset_id", sa.Integer(), nullable=False),
        sa.Column("firmware_id", sa.Integer(), nullable=False),
        sa.Column("filename", sa.Text(), nullable=False),
        sa.Column("ctime", sa.DateTime(), nullable=False),
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["firmware_id"],
            ["firmware.firmware_id"],
        ),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["users.user_id"],
        ),
        sa.PrimaryKeyConstraint("firmware_asset_id"),
    )
    with op.batch_alter_table("firmware_assets", schema=None) as batch_op:
        batch_op.create_index(
            batch_op.f("ix_firmware_assets_firmware_id"), ["firmware_id"], unique=False
        )


def downgrade():
    with op.batch_alter_table("firmware_assets", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_firmware_assets_firmware_id"))
    op.drop_table("firmware_assets")
