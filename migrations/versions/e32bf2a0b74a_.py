"""empty message

Revision ID: e32bf2a0b74a
Revises: 6785bd282c53
Create Date: 2023-01-01 17:04:12.419677

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "e32bf2a0b74a"
down_revision = "6785bd282c53"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("firmware", schema=None) as batch_op:
        batch_op.drop_column("ipfs")
        batch_op.drop_column("regenerate_ts")
        batch_op.drop_column("is_dirty")
    with op.batch_alter_table("remotes", schema=None) as batch_op:
        batch_op.drop_column("is_dirty")
        batch_op.drop_column("regenerate_ts")


def downgrade():
    with op.batch_alter_table("remotes", schema=None) as batch_op:
        batch_op.add_column(
            sa.Column(
                "regenerate_ts",
                postgresql.TIMESTAMP(),
                autoincrement=False,
                nullable=True,
            )
        )
        batch_op.add_column(
            sa.Column("is_dirty", sa.BOOLEAN(), autoincrement=False, nullable=True)
        )
    with op.batch_alter_table("firmware", schema=None) as batch_op:
        batch_op.add_column(
            sa.Column("is_dirty", sa.BOOLEAN(), autoincrement=False, nullable=True)
        )
        batch_op.add_column(
            sa.Column(
                "regenerate_ts",
                postgresql.TIMESTAMP(),
                autoincrement=False,
                nullable=True,
            )
        )
        batch_op.add_column(
            sa.Column("ipfs", sa.TEXT(), autoincrement=False, nullable=True)
        )
