#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-few-public-methods,unused-argument,protected-access

from typing import List, Optional

from lvfs.tasks.models import Task
from .models import Protocol


def _fsck_protocols_update_message(self: Protocol, task: Task) -> None:

    # per-protocol hardcoded update request id
    update_request_ids = {
        "com.dell.dock": "org.freedesktop.fwupd.request.remove-usb-cable",
        "org.uefi.capsule": "org.freedesktop.fwupd.request.do-not-power-off",
        "tw.com.ite.superio": "org.freedesktop.fwupd.request.do-not-power-off",
    }
    if self.value in update_request_ids:
        if self.update_request_id != update_request_ids[self.value]:
            self.update_request_id = update_request_ids[self.value]
            task.add_fail(
                "Database::Protocol",
                "Fixed up {} update request ID".format(self.value),
            )

    # per-protocol hardcoded update message
    update_messages = {
        "com.dell.dock": "The update will continue when the device USB cable has been unplugged.",
        "org.uefi.capsule": "Do not turn off your computer or remove the AC adapter "
        "while the update is in progress.",
        "tw.com.ite.superio": "Do not turn off your computer or remove the AC adapter "
        "while the update is in progress.",
    }
    if self.value in update_messages:
        if self.update_message != update_messages[self.value]:
            self.update_message = update_messages[self.value]
            self.allow_custom_update_message = False
            task.add_fail(
                "Database::Protocol",
                "Fixed up {} update message".format(self.value),
            )

    # protocols
    if not self.allow_custom_update_message and self.value in [
        "com.cypress.ccgx",
        "com.cypress.ccgx.dmc",
        "com.infineon.ccgx",
        "com.infineon.ccgx.dmc",
    ]:
        self.allow_custom_update_message = True
        task.add_fail(
            "Database::Protocol",
            "Fixed up {} update message".format(self.value),
        )


def _fsck(self: Protocol, task: Task, kinds: Optional[List[str]] = None) -> None:

    if not kinds or "update-message" in kinds:
        _fsck_protocols_update_message(self, task)
