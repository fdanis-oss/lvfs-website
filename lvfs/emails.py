#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+

import json
from flask import g
from flask import current_app as app
from flask_mail import Message

from lvfs import mail, db
from lvfs.tasks.models import Task


def send_email_sync(subject: str, recipient: str, text_body: str, task: Task) -> None:
    if "MAIL_SUPPRESS_SEND" in app.config and app.config["MAIL_SUPPRESS_SEND"]:
        if "DEBUG" in app.config and app.config["DEBUG"]:
            # also save the email *contents* -- which could be password...
            task.add_fail("Not sending email to {}".format(recipient), text_body)
        else:
            task.add_fail("Not sending email to {}".format(recipient))
        return
    task.add_pass("Sending email to {}".format(recipient))
    msg = Message(subject, recipients=[recipient])
    msg.body = text_body
    mail.send(msg)


def task_send_email(task: Task) -> None:
    values = json.loads(task.value)
    send_email_sync(
        values["subject"], values["recipient"], values["text_body"], task=task
    )


def send_email(subject: str, recipient: str, text_body: str) -> None:
    if hasattr(g, "user"):
        user_id = g.user.user_id
    else:
        user_id = 2
    db.session.add(
        Task(
            value=json.dumps(
                {"subject": subject, "recipient": recipient, "text_body": text_body}
            ),
            caller=__name__,
            user_id=user_id,
            function="lvfs.emails.task_send_email",
        )
    )
    db.session.commit()
