#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-few-public-methods,unused-argument,protected-access

from typing import List, Optional

from lvfs import db
from lvfs.tasks.models import Task
from .models import Report, REPORT_ATTR_MAP
from .utils import _report_key_valid


def _fsck_reports_fixup_old_attrs(self: Report, task: Task) -> None:

    # fix up report attrs
    fixed: List[str] = []
    for key, value in REPORT_ATTR_MAP.items():
        attr = self.get_attribute_by_key(key)
        if attr:
            attr.key = value
            if key not in fixed:
                fixed.append(key)
    if fixed:
        task.add_fail(
            "Database::Reports",
            "Fixed up {} keys".format(", ".join(fixed)),
        )


def _fsck_reports_fixup_invalid_attrs(self: Report, task: Task) -> None:

    # delete crazy values
    for attr in self.attributes:
        if not _report_key_valid(attr.key):
            task.add_fail(
                "Database::Reports",
                "Deleted {}={}".format(attr.key, attr.value),
            )
            db.session.delete(attr)


def _fsck(self: Report, task: Task, kinds: Optional[List[str]] = None) -> None:

    if not kinds or "attrs" in kinds:
        _fsck_reports_fixup_old_attrs(self, task)
        _fsck_reports_fixup_invalid_attrs(self, task)
