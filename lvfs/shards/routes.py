#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2019 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+

from collections import defaultdict
from typing import Dict, List, Any

from uuid import UUID


from flask import (
    Blueprint,
    flash,
    make_response,
    redirect,
    render_template,
    request,
    url_for,
)
from flask_login import login_required
from sqlalchemy.exc import NoResultFound, IntegrityError

from lvfs import db

from lvfs.components.models import (
    Component,
    ComponentShard,
    ComponentShardClaim,
    ComponentShardInfo,
)
from lvfs.claims.models import Claim
from lvfs.util import admin_login_required


bp_shards = Blueprint("shards", __name__, template_folder="templates")


@bp_shards.route("/")
@login_required
@admin_login_required
def route_list() -> Any:

    # only show shards with the correct group_id
    shards = (
        db.session.query(ComponentShardInfo)
        .order_by(ComponentShardInfo.cnt.desc())
        .all()
    )
    return render_template("shard-list.html", category="admin", shards=shards)


@bp_shards.post("/create")
@login_required
@admin_login_required
def route_create() -> Any:
    # ensure has enough data
    if "guid" not in request.form:
        flash("No form data found", "warning")
        return redirect(url_for("shards.route_list"))
    guid = request.form["guid"].lower()
    try:
        _ = UUID(guid)
    except ValueError:
        flash("Failed to add shard: Not a GUID", "warning")
        return redirect(url_for("shards.route_list"))

    # add ComponentShardInfo
    try:
        info = ComponentShardInfo(guid=guid)
        db.session.add(info)
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("Failed to add shard: Already exists", "info")
        return redirect(url_for("shards.route_list"))
    flash("Added shard", "info")
    return redirect(
        url_for(
            "shards.route_show", component_shard_info_id=info.component_shard_info_id
        )
    )


@bp_shards.post("/<int:component_shard_info_id>/modify")
@login_required
@admin_login_required
def route_modify(component_shard_info_id: int) -> Any:

    # find shard
    try:
        shard = (
            db.session.query(ComponentShardInfo)
            .filter(
                ComponentShardInfo.component_shard_info_id == component_shard_info_id
            )
            .with_for_update(of=ComponentShardInfo)
            .one()
        )
    except NoResultFound:
        flash("No shard found", "info")
        return redirect(url_for("shards.route_list"))

    # modify shard
    for key in ["description", "claim_id"]:
        if key in request.form:
            setattr(shard, key, request.form[key] or None)
    db.session.commit()

    # success
    flash("Modified shard", "info")
    return route_show(component_shard_info_id)


@bp_shards.post("/<int:component_shard_info_id>/delete")
@login_required
@admin_login_required
def route_delete(component_shard_info_id: int) -> Any:

    try:
        db.session.query(ComponentShardInfo).filter(
            ComponentShardInfo.component_shard_info_id == component_shard_info_id
        ).delete()
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("Cannot delete", "danger")
        return redirect(url_for("shards.route_list"))
    flash("Deleted shard", "info")
    return redirect(url_for("shards.route_list"))


@bp_shards.route("/<int:component_shard_info_id>/details")
@login_required
@admin_login_required
def route_show(component_shard_info_id: int) -> Any:

    # find shard
    try:
        shard = (
            db.session.query(ComponentShardInfo)
            .filter(
                ComponentShardInfo.component_shard_info_id == component_shard_info_id
            )
            .one()
        )
    except NoResultFound:
        flash("No shard found", "info")
        return redirect(url_for("shards.route_list"))

    # show details
    claims = db.session.query(Claim).order_by(Claim.summary).all()
    return render_template(
        "shard-details.html", category="admin", claims=claims, shard=shard
    )


@bp_shards.route("/<int:component_shard_info_id>/components")
@login_required
@admin_login_required
def route_components(component_shard_info_id: int) -> Any:

    # find shard
    try:
        shard_info = (
            db.session.query(ComponentShardInfo)
            .filter(
                ComponentShardInfo.component_shard_info_id == component_shard_info_id
            )
            .one()
        )
    except NoResultFound:
        flash("No shard found", "info")
        return redirect(url_for("shards.route_list"))

    # get shards with a unique component appstream ID
    shards = (
        db.session.query(ComponentShard)
        .join(ComponentShardInfo)
        .filter(ComponentShardInfo.component_shard_info_id == component_shard_info_id)
        .join(Component)
        .distinct(Component.appstream_id)
        .order_by(Component.appstream_id, ComponentShard.name.desc())
        .all()
    )
    unique_mds: Dict[str, List[Component]] = defaultdict(list)
    for shard in shards:
        if shard.md not in unique_mds[shard.name]:
            unique_mds[shard.name].append(shard.md)

    # show details
    return render_template(
        "shard-components.html",
        category="admin",
        unique_mds=unique_mds,
        shard=shard_info,
    )


@bp_shards.route("/<int:component_shard_info_id>/checksums")
@login_required
@admin_login_required
def route_checksums(component_shard_info_id: int) -> Any:

    # find shard
    try:
        shard_info = (
            db.session.query(ComponentShardInfo)
            .filter(
                ComponentShardInfo.component_shard_info_id == component_shard_info_id
            )
            .one()
        )
    except NoResultFound:
        flash("No shard found", "info")
        return redirect(url_for("shards.route_list"))

    # get shards with a unique component appstream ID
    shards_by_checksum: Dict[str, List[ComponentShard]] = defaultdict(list)
    for shard in (
        db.session.query(ComponentShard)
        .join(ComponentShardInfo)
        .filter(ComponentShardInfo.component_shard_info_id == component_shard_info_id)
    ):
        shards_by_checksum[shard.checksum].append(shard)

    # show details
    return render_template(
        "shard-checksums.html",
        category="admin",
        shards_by_checksum=shards_by_checksum,
        shard=shard_info,
    )


@bp_shards.route("/<int:component_shard_info_id>/claims")
@login_required
@admin_login_required
def route_claims(component_shard_info_id: int) -> Any:

    # find shard
    try:
        shard = (
            db.session.query(ComponentShardInfo)
            .filter(
                ComponentShardInfo.component_shard_info_id == component_shard_info_id
            )
            .one()
        )
    except NoResultFound:
        flash("No shard found", "info")
        return redirect(url_for("shards.route_list"))

    # get claims for this ComponentShardInfo
    claims = (
        db.session.query(ComponentShardClaim)
        .filter(ComponentShardClaim.component_shard_info_id == component_shard_info_id)
        .all()
    )

    # show details
    claims_all = db.session.query(Claim).order_by(Claim.summary).all()
    return render_template(
        "shard-claims.html",
        category="admin",
        claims=claims,
        claims_all=claims_all,
        shard=shard,
    )


@bp_shards.route(
    "/<int:component_shard_info_id>/claim/<int:component_shard_claim_id>/delete",
    methods=["POST"],
)
@login_required
@admin_login_required
def route_shard_claim_delete(
    component_shard_info_id: int, component_shard_claim_id: int
) -> Any:

    # find shard
    try:
        db.session.query(ComponentShardClaim).filter(
            ComponentShardClaim.component_shard_info_id == component_shard_info_id
        ).filter(
            ComponentShardClaim.component_shard_claim_id == component_shard_claim_id
        ).delete()
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("Cannot delete", "danger")
        return redirect(url_for("shards.route_list"))
    flash("Deleted shard claim", "info")
    return redirect(
        url_for("shards.route_claims", component_shard_info_id=component_shard_info_id)
    )


@bp_shards.post("/<int:component_shard_info_id>/claim/create")
@login_required
@admin_login_required
def route_shard_claim_create(component_shard_info_id: int) -> Any:

    # ensure has enough data
    for key in ["checksum", "claim_id"]:
        if key not in request.form:
            flash("No {} form data found!".format(key), "warning")
            return redirect(url_for("shards.route_list"))

    # add issue
    try:
        claim = ComponentShardClaim(
            component_shard_info_id=component_shard_info_id,
            checksum=request.form["checksum"],
            claim_id=request.form["claim_id"],
        )
        db.session.add(claim)
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash(
            "Failed to add component shard claim: The checksum already exists", "info"
        )
        return redirect(url_for("issues.route_list"))
    flash("Added claim", "info")
    return redirect(
        url_for("shards.route_claims", component_shard_info_id=component_shard_info_id)
    )


@bp_shards.route("/<int:component_shard_id>/download")
@login_required
def route_download(component_shard_id: int) -> Any:

    # find shard
    try:
        shard = (
            db.session.query(ComponentShard)
            .filter(ComponentShard.component_shard_id == component_shard_id)
            .one()
        )
    except NoResultFound:
        flash("No shard found", "info")
        return redirect(url_for("shards.route_list"))
    if not shard.md.fw.check_acl("@view"):
        flash("Permission denied: Unable to download shard", "danger")
        return redirect(url_for("main.route_dashboard"))
    if not shard.blob:
        flash("Permission denied: Shard has no data", "warning")
        return redirect(url_for("main.route_dashboard"))
    response = make_response(shard.blob)
    response.headers.set("Content-Type", "application/octet-stream")
    response.headers.set(
        "Content-Disposition",
        "attachment",
        filename="{}-{}".format(shard.guid, shard.checksum),
    )
    return response
