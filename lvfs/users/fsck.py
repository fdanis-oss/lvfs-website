#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-few-public-methods,unused-argument,protected-access

from typing import List, Optional
from sqlalchemy.exc import IntegrityError

from lvfs.tasks.models import Task, TaskScheduler
from lvfs.components.models import ComponentIssue, ComponentDescription
from lvfs.devices.models import Product
from lvfs.firmware.models import FirmwareEvent, FirmwareVendor, FirmwareAction, Firmware
from lvfs.hsireports.models import HsiReport
from lvfs.main.models import Event
from lvfs.reports.models import Report
from lvfs.queries.models import YaraQuery
from lvfs.tests.models import Test
from lvfs.vendors.models import (
    VendorAffiliationAction,
    VendorAsset,
    VendorBranch,
    VendorNamespace,
    VendorProdcert,
    VendorTag,
)
from lvfs import db

from .models import User, UserCertificate, UserAction, UserToken, UserGroup


def _fsck_user_fixup_subgroup(self: User, task: Task) -> None:
    """fix up user subgroups"""

    # already set
    if self.subgroup:
        return

    open_idx: int = self.display_name.find("(")
    close_idx: int = self.display_name.find(")")
    if open_idx == -1 or open_idx == -1 or open_idx > close_idx:
        return

    self.subgroup = self.display_name[open_idx + 1 : close_idx]
    self.display_name = self.display_name[:open_idx].strip()
    task.add_fail(
        "Database::Users",
        "Set subgroup of {} for user {}".format(self.subgroup, self.user_id),
    )


def _fsck_user_merge(old_user: User, new_user: User, task: Task) -> None:

    try:
        for item in db.session.query(ComponentIssue).filter(
            ComponentIssue.user_id == old_user.user_id
        ):
            item.user_id = new_user.user_id
        for item in db.session.query(ComponentDescription).filter(
            ComponentDescription.user_id == old_user.user_id
        ):
            item.user_id = new_user.user_id
        for item in db.session.query(Product).filter(
            Product.user_id == old_user.user_id
        ):
            item.user_id = new_user.user_id
        for item in db.session.query(FirmwareEvent).filter(
            FirmwareEvent.user_id == old_user.user_id
        ):
            item.user_id = new_user.user_id
        for item in db.session.query(FirmwareVendor).filter(
            FirmwareVendor.user_id == old_user.user_id
        ):
            item.user_id = new_user.user_id
        for item in db.session.query(FirmwareAction).filter(
            FirmwareAction.user_id == old_user.user_id
        ):
            item.user_id = new_user.user_id
        for item in db.session.query(Firmware).filter(
            Firmware.user_id == old_user.user_id
        ):
            item.user_id = new_user.user_id
        for item in db.session.query(HsiReport).filter(
            HsiReport.user_id == old_user.user_id
        ):
            item.user_id = new_user.user_id
        for item in db.session.query(Event).filter(Event.user_id == old_user.user_id):
            item.user_id = new_user.user_id
        for item in db.session.query(YaraQuery).filter(
            YaraQuery.user_id == old_user.user_id
        ):
            item.user_id = new_user.user_id
        for item in db.session.query(Report).filter(Report.user_id == old_user.user_id):
            item.user_id = new_user.user_id
        for item in db.session.query(TaskScheduler).filter(
            TaskScheduler.user_id == old_user.user_id
        ):
            item.user_id = new_user.user_id
        for item in db.session.query(Task).filter(Task.user_id == old_user.user_id):
            item.user_id = new_user.user_id
        for item in db.session.query(Test).filter(
            Test.waived_user_id == old_user.user_id
        ):
            item.waived_user_id = new_user.user_id
        for item in db.session.query(VendorAffiliationAction).filter(
            VendorAffiliationAction.user_id == old_user.user_id
        ):
            item.user_id = new_user.user_id
        for item in db.session.query(VendorBranch).filter(
            VendorBranch.user_id == old_user.user_id
        ):
            item.user_id = new_user.user_id
        for item in db.session.query(VendorNamespace).filter(
            VendorNamespace.user_id == old_user.user_id
        ):
            item.user_id = new_user.user_id
        for item in db.session.query(VendorTag).filter(
            VendorTag.user_id == old_user.user_id
        ):
            item.user_id = new_user.user_id
        for item in db.session.query(VendorAsset).filter(
            VendorAsset.user_id == old_user.user_id
        ):
            item.user_id = new_user.user_id
        for item in db.session.query(VendorProdcert).filter(
            VendorProdcert.user_id == old_user.user_id
        ):
            item.user_id = new_user.user_id
        for item in db.session.query(UserCertificate).filter(
            UserCertificate.user_id == old_user.user_id
        ):
            item.user_id = new_user.user_id
        for item in db.session.query(UserAction).filter(
            UserAction.user_id == old_user.user_id
        ):
            item.user_id = new_user.user_id
        for item in db.session.query(UserToken).filter(
            UserToken.user_id == old_user.user_id
        ):
            item.user_id = new_user.user_id
        for item in db.session.query(UserGroup).filter(
            UserGroup.user_id == old_user.user_id
        ):
            item.user_id = new_user.user_id
        db.session.query(User).filter(User.user_id == old_user.user_id).delete()
        db.session.commit()
    except IntegrityError as e:
        db.session.rollback()
        task.add_fail(
            "Database::Users",
            "Cannot merge user {} into {}: {}".format(old_user, new_user, str(e)),
        )
    else:
        task.add_pass(
            "Database::Users",
            "Merged user {} into {}".format(old_user, new_user),
        )


def _fsck_user_fixup_human_duplicate(self: User, task: Task) -> None:
    """fix up human_user_id"""
    if self.user_id == self.human_user_id:
        self.human_user_id = None
        task.add_pass(
            "Database::Users",
            "Removed duplicate human user {}".format(self),
        )


def _fsck_user_fixup_duplicates(self: User, task: Task) -> None:
    """fix up user subgroups"""

    # find duplicate
    user = (
        db.session.query(User)
        .filter(User.user_id != self.user_id)
        .filter(User.username == self.username)
        .first()
    )
    if not user:
        return

    # which one is deleted
    if user.auth_type == "disabled":
        _fsck_user_merge(user, self, task=task)
    elif self.auth_type == "disabled":
        _fsck_user_merge(self, user, task=task)
    else:
        task.add_fail(
            "Database::Users",
            "Cannot merge user {} into {}: Neither user is disabled".format(user, self),
        )


def _fsck(self: User, task: Task, kinds: Optional[List[str]] = None) -> None:

    if not kinds or "duplicates" in kinds:
        _fsck_user_fixup_duplicates(self, task)
        _fsck_user_fixup_human_duplicate(self, task)
    if not kinds or "subgroup" in kinds:
        _fsck_user_fixup_subgroup(self, task)
